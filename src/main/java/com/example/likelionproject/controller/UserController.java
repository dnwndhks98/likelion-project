package com.example.likelionproject.controller;

import com.example.likelionproject.entity.Response;
import com.example.likelionproject.entity.dto.UserJoinRequest;
import com.example.likelionproject.entity.dto.UserJoinResponse;
import com.example.likelionproject.entity.dto.UserLoginRequest;
import com.example.likelionproject.entity.dto.UserLoginResponse;
import com.example.likelionproject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinResponse> add(@RequestBody UserJoinRequest dto) {

        return new Response<>("SUCCESS", userService.add(dto));
    }

//    @PostMapping("/login")
//    public UserLoginResponse add(@RequestBody UserLoginRequest dto) {
//        return userService.login(dto);
//    }
}
