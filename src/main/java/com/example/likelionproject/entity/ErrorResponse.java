package com.example.likelionproject.entity;

import com.example.likelionproject.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorResponse {
    private com.example.likelionproject.exception.ErrorCode ErrorCode;
    private String message;


}

