package com.example.likelionproject.entity.dto;

import com.example.likelionproject.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinResponse {
    private Long userId;
    private String userName;

    public static UserJoinResponse of(User user) {
        return UserJoinResponse.builder()
                .userId(user.getUserId())
                .userName(user.getUserName())
                .build();
    }
}
