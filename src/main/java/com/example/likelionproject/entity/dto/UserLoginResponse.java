package com.example.likelionproject.entity.dto;

import com.example.likelionproject.entity.User;
import lombok.Builder;

@Builder
public class UserLoginResponse {

    private String JWT;

    public UserLoginResponse(String JWT) {
        this.JWT = JWT;
    }

}
