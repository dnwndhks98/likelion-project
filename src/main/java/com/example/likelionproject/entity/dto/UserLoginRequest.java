package com.example.likelionproject.entity.dto;

import lombok.Getter;

@Getter
public class UserLoginRequest {

    private String userName;
    private String password;

}
