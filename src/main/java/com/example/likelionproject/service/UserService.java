package com.example.likelionproject.service;

import com.example.likelionproject.entity.User;
import com.example.likelionproject.entity.dto.UserJoinRequest;
import com.example.likelionproject.entity.dto.UserJoinResponse;
import com.example.likelionproject.entity.dto.UserLoginRequest;
import com.example.likelionproject.entity.dto.UserLoginResponse;
import com.example.likelionproject.exception.AppException;
import com.example.likelionproject.exception.ErrorCode;
import com.example.likelionproject.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    public UserJoinResponse add(UserJoinRequest dto) {

        userRepository.findByUserName(dto.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME,ErrorCode.DUPLICATED_USER_NAME.getMessage());
                });

        User user = userRepository.save(dto.toEntity());

        return UserJoinResponse.of(user);
    }



}
